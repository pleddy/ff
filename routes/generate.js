"use strict";

var fs = require('fs');
var _ = require('underscore')
var express = require('express');
var router = express.Router();
var lang_service = require('../services/lang-service')

router.get('/supersets', supersets)
router.post('/supersets', supersets)

module.exports = router;

function supersets(req, res, next) {

  //console.log('body: ', req.body)
  //console.log('params: ',req.params)
  //console.log('query: ',req.query)

  var m_lang = req.session.lang
  var m_mylang = req.session.mylang

  var m_delay = req.body.delay
  var m_loops = req.body.loops
  var m_sets_checked = req.body.sets_checked

  if ( typeof m_sets_checked !== "undefined" ) {
    var m_name = get_generated_name(m_sets_checked)
    console.log('name returned: ', m_name)
    var m_cmd = "sox -G "
    var m_mp3s = _.map(m_sets_checked, function(m_set_tmp){
      m_cmd += "set" + m_set_tmp + ".mp3"
      m_cmd += " "
      m_cmd += "s" + m_delay + ".mp3"
      m_cmd += " "
    })
    m_cmd += m_name + " tempo 0.75"
    console.log(m_cmd)
  }

  var m_options = {}
  m_options = {
    m_delay: req.body.delay,
    m_loops: req.body.loops,
    m_sets: req.body.sets_checked
  }

  fs.readFile('public/word_sets_' + m_lang + '.txt', 'utf8', function (err, m_data) {
    if (err) {return console.log(err); }
    var m_sets = m_data.split('\n')
    var m_sets_result = _.map(m_sets, function(m_set){ return m_set.split(' - '); });
    var m_words = []
    _.each(m_sets, function(m_set){ 
      var tmp_array = m_set.split(' - '); 
      m_words = m_words.concat(tmp_array)
    });
    var m_map = lang_service.get_lang_map(m_words, m_lang) 
    res.render('generate/supersets', { title: 'FF - Generator', m_sets: m_sets_result, m_map: m_map, m_lang: m_lang, m_mylang: m_mylang, m_options: m_options });
  });

}

function get_generated_name(m_sets_checked) {
  var m_last
  var m_previous
  var m_name_tmp
  if (m_sets_checked.length == 1) {
    return 'gset' + m_sets_checked[0] + '.mp3'
  } else if (m_sets_checked.length == 2) {
    if ( m_sets_checked[0] == (m_sets_checked[1] - 1)  ) {
      return 'gset' + m_sets_checked[0] + '-' + m_sets_checked[1] +  '.mp3'
    } else {
      return 'gset' + m_sets_checked[0] + '_' + m_sets_checked[1] +  '.mp3'
    }
  } else {
    var m_index = 0
    _.each(m_sets_checked, function(m_set) {
      // first
      if (m_index == 0) m_name_tmp += m_set
      if ( m_sets_checked.length == m_index ) {
        return m_name_tmp + m_symbol + m_set
      }
      // one greater than last
      if ( m_sets_checked[m_index - 1] == m_set ) {
        m_last = m_set
        m_symbol = '-'
      } else {
        m_iternator = '_'
      }
      // two or more greater than last
      // last
      m_index++    
    })
  }
}
