"use strict";

var fs = require('fs');
var _ = require('underscore')
var express = require('express');
var router = express.Router();
var lang_service = require('../services/lang-service')

router.post('/', sharedHandler)
router.get('/', sharedHandler)
router.get('/audio_list', sharedHandler)

module.exports = router;

function sharedHandler(req, res, next) {

  //console.log('body: ', req.body)
  //console.log('params: ',req.params)
  //console.log('query: ',req.query)

  var m_lang
  if ( typeof req.session.lang === 'undefined') {
    req.session.lang = 'fr' 
    m_lang = 'fr'
  } else if ( typeof req.query.lang !== 'undefined' ) {
    m_lang = req.query.lang
    req.session.lang = req.query.lang
  } else if ( typeof req.body.lang === 'undefined' ) {
    m_lang = req.session.lang
  } else {
    req.session.lang = req.body.lang
    m_lang = req.session.lang
  }

  var m_mylang
//  console.log('Before: ', req.session.mylang)
  if ( typeof req.session.mylang === 'undefined') {
    // first hit
//    console.log('first hit')
    req.session.mylang = 'en' 
    m_mylang = 'en'
  } else if ( typeof req.body.mylang === 'undefined' ) {
    // reload or other form submit
//    console.log('reload or other form submit')
    m_mylang = req.session.mylang
  } else {
    // form submit
 //   console.log('my form submit')
    req.session.mylang = req.body.mylang
    m_mylang =  req.body.mylang
  }
//  console.log('After: ', m_mylang)

  var m_page
  var m_options = {}
  if ( req.path == '/audio_list' ) {
    m_page = 'audio_list'
  } else if ( req.path == '/generate/supersets' ) {
    m_options = {
      m_delay: req.body.delay,
      m_loops: req.body.loops,
      m_sets: req.body.sets_checked
    }
    m_page = 'generate/supersets'
  } else {
    m_page = 'index'
  }

  fs.readFile('public/word_sets_' + m_lang + '.txt', 'utf8', function (err, m_data) {
    if (err) {return console.log(err); }
    var m_sets = m_data.split('\n')
    var m_sets_result = _.map(m_sets, function(m_set){ return m_set.split(' - '); });
    var m_words = []
    _.each(m_sets, function(m_set){ 
      var tmp_array = m_set.split(' - '); 
      m_words = m_words.concat(tmp_array)
    });
    var m_map = lang_service.get_lang_map(m_words, m_lang) 
    res.render(m_page, { title: 'FF', m_sets: m_sets_result, m_map: m_map, m_lang: m_lang, m_mylang: m_mylang, m_options: m_options });
  });

}
