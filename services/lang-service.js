var _ = require('underscore')


var get_lang_map = function(m_words, m_lang) {
  var m_map = {}
  if (m_lang == 'fr') {
    _.each(m_words, function(m_word) {
      var m_audio
      m_audio = m_word.replace(/[èéê]/g, 'e')
      m_audio = m_audio.replace(/[ ]/g, '_')
      m_audio = m_audio.replace(/[ -]/g, '_')
      m_audio = m_audio.replace(/[î]/g, 'i')
      m_audio = m_audio.replace(/[ô]/g, 'o')
      m_audio = m_audio.replace(/[œ]/g, 'o')
      m_audio = m_audio.replace(/[û]/g, 'u')
      m_audio = m_audio.replace(/[’]/g, '')
      m_audio = m_audio.replace(/[ç]/g, 'c')
      m_audio = m_audio.replace(/[ï]/g, 'i')
      m_audio = m_audio.replace(/[â]/g, 'a')
      m_map[m_word] = m_audio
    })
    return m_map
  }
  if (m_lang == 'ru') {
    m_map = get_ru_map()
    return m_map
  }
}


var get_ru_map = function() {
  return {
"ад":  "ad",
"Бог":  "Bog",
"аэропорт":  "aeroport", "актер":  "akter", "апельсин":  "apelsin", "апрель":  "aprel", "армия":  "armija", "атака":  "ataka", "август":  "avgust", "автобус":  "avtobus", "автор":  "avtor",
"бабушка":  "babuska", "банан":  "banan", "банк":  "bank", "бар":  "bar", "бассейн":  "bassejn", "бедный":  "bednyj", "бегать":  "begat", "белый":  "belyj", "бензин":  "benzin", "библиотека":  "biblioteka", "билет":  "bilet", "бить":  "bit", "богатый":  "bogatyj", "боль":  "bol", "больница":  "bolnica", "больной":  "bolnoj", "большой":  "bolsoj", "болельщик":  "bolelscik", "болезнь":  "bolezn", "борода":  "boroda", "брак":  "brak", "брат":  "brat", "бриллиант":  "brilliant", "бросать":  "brosat", "брюки":  "brjuki", "бумага":  "bumaga", "бутылка":  "butylka", "быстрый":  "bystryj",
"час":  "cas", "чашка":  "caska", "часы":  "casy", "чай":  "caj", "человек":  "celovek", "черный":  "cernyj", "четверг":  "cetverg", "четвертый":  "cetvertyj", "четыре":  "cetyre", "четырнадцать":  "cetyrnadcat", "чистить":  "cistit", "чистый":  "cistyj",
"cмешивать":  "cmesivat",
"да":  "da", "дата":  "data", "дедушка":  "deduska", "декабрь":  "dekabr", "день":  "den", "деньги":  "dengi", "дерево":  "derevo", "дешевый":  "desevyj", "десять":  "desjat", "десять тысяч":  "desjattysjac", "девочка":  "devocka", "девяносто":  "devjanosto", "девяносто два":  "devjanostodva", "девяносто один":  "devjanostoodin", "девять":  "devjat", "девятнадцать":  "devjatnadcat", "длинный":  "dlinnyj", "дочь":  "doc", "долина":  "dolina", "доллар":  "dollar", "дом":  "dom", "дорога":  "doroga", "дорогой":  "dorogoj", "дождь":  "dozd", "драться":  "dratsja", "друг":  "drug", "думать":  "dumat", "два":  "dva", "два": "dva", "игрок":  "igrok", "двадцать":  "dvadcat", "двадцать один":  "dvadcatodin", "двенадцать":  "dvenadcat", "дверь":  "dver", "двор":  "dvor", "дырка":  "dyrka", "дюйм":  "djujm",
"экран":  "ekran", "энергия":  "energija", "этаж":  "etaz", "закрывать":  "zakryvat", "заметка":  "zametka", "замок":  "zamok", "запад":  "zapad", "завтрак":  "zavtrak", "здание":  "zdanie", "здоровый":  "zdorovyj", "зеленый":  "zelenyj", "земля":  "zemlja",
"ферма":  "ferma", "февраль":  "fevral", "фильм":  "film", "фотоаппарат":  "fotoapparat", "фотография":  "fotografija", "фунт":  "funt", "фут":  "fut", "футболка":  "futbolka",
"газета":  "gazeta", "гласный звук":  "glasnyjzvuk", "глаз":  "glaz", "глина":  "glina", "глубокий":  "glubokij", "глухой":  "gluxoj", "гнуть":  "gnut", "год":  "god", "голос":  "golos", "голова":  "golova", "голубой":  "goluboj", "гонка":  "gonka", "гора":  "gora", "гореть":  "goret", "город":  "gorod", "городок":  "gorodok", "гостиница":  "gostinica", "говорить":  "govorit", "готовить": "gotovit", "говядина":  "govjadina", "громкий":  "gromkij", "группа":  "gruppa", "грустный":  "grustnyj", "грузовик":  "gruzovik", "грязный":  "grjaznyj", "губа":  "guba",
"идти":  "idti", "иглa":  "igla", "игра":  "igra", "играть":  "igrat", "инструмент":  "instrument", "искусcтво":  "iskusstvo", "июнь":  "ijun", "июль": "ijul", "изображение":  "izobrazenie", "изогнутый":  "izognutyj", "известный":  "izvestnyj",
"кабинет":  "kabinet", "капать":  "kapat", "карандаш":  "karandas", "карман":  "karman", "карта":  "karta", "килограмм":  "kilogramm", "клуб":  "klub", "ключ":  "kljuc", "книга":  "kniga", "кофе":  "kofe", "кольцо":  "kolco", "коленo":  "koleno", "команда":  "komanda", "комната":  "komnata", "компьютер":  "kompjuter", "контракт":  "kontrakt", "корабль":  "korabl", "корень":  "koren", "коричневый":  "koricnevyj", "кормить":  "kormit", "коробка":  "korobka", "король":  "korol", "королева":  "koroleva", "кoроткий":  "korotkij", "корова":  "korova", "кошка":  "koska", "космос":  "kosmos", "кость":  "kost", "костюм":  "kostjum", "кожа":  "koza", "красивый":  "krasivyj", "краска":  "kraska", "красный":  "krasnyj", "кровь":  "krov", "кровать":  "krovat", "круг":  "krug", "крыло":  "krylo", "крыша":  "krysa", "кухня":  "kuxnja", "кукуруза":  "kukuruza", "курица":  "kurica", "кусок":  "kusok", "квадрат":  "kvadrat", "квартира":  "kvartira",
"хлеб":  "xleb", "ходить":  "xodit", "холм":  "xolm", "холодный":  "xolodnyj", "хороший":  "xorosij", "художник":  "artist",
"лагерь":  "lager", "лампа":  "lampa", "лечь":  "lect", "лед":  "led", "легкий":  "legkij", "лекарство":  "lekarstvo", "лэптоп":  "leptop", "лес":  "les", "летать":  "letat", "лето":  "leto", "левый":  "levyj", "лимон":  "limon", "лист":  "list", "лицо":  "lico", "лодка":  "lodka", "ломать":  "lomat", "лошадь":  "losad", "ловить":  "lovit", "ложка":  "lozka", "луна":  "luna", "любезный":  "ljubeznyj", "любить":  "ljubit",
"магазин":  "magazin1", "мальчик":  "malcik", "маленький":  "malenkij", "маленького роста":  "malinkabarosta", "март":  "mart", "машина":  "masina", "масло":  "maslo", "мать":  "mat", "материал":  "material", "май":  "maj", "медь":  "med", "медленный":  "medlennyj", "менеджер":  "menedzer", "мертвый":  "mertvyj", "мешок":  "mesok", "местоположениe":  "mestopolozenie", "месяц":  "mesjac", "металл":  "metall", "метр":  "metr", "миллиард":  "milliard", "миллион":  "million", "минутa":  "minuta", "мир":  "mir", "мобильный":  "mobilnyj", "мокрый":  "mokryj", "молитьcя":  "molitsja", "молодой":  "molodoj", "молоко":  "moloko", "море":  "more", "мост":  "most", "мотор":  "motor", "мозг":  "mozg", "муж":  "muz", "мужчина":  "muzcina", "мужской":  "muzskoj", "музыка":  "muzyka", "музыкальный инструмент":  "muzykalnyjinstrument", "мы":  "my", "мяч":  "mjac", "мягкий":  "mjagkij", "мыло":  "mylo", "мышь":  "mys", "мыть":  "myt",
"находить":  "naxodit", "напиток":  "napitok", "направление":  "napravlenie", "наркотик":  "narkotik", "наука":  "nauka", "назад":  "nazad", "небесa":  "nebesa", "небо":  "nebo", "неделя":  "nedelja", "неглубокий":  "neglubokij", "нет":  "net", "низ":  "niz", "низкий":  "nizkij", "край":  "kraj", "ночь":  "noc", "нога":  "noga", "ноль":  "nol", "номер":  "nomer", "нос":  "nos", "носить":  "nosit", "новый":  "novyj", "ноябрь":  "nojabr", "нож":  "noz",
"обед":  "obed", "одежда":  "obrazec", "один":  "odezda", "один":  "odin", "одиннадцать":  "odinnadcat", "официант":  "oficiant", "огонь":  "ogon", "океан":  "okean", "окно":  "okno", "октябрь":  "oktjabr", "он":  "on", "она":  "ona", "они":  "oni", "оно":  "ono", "оранжевый":  "oranzevyj", "осень":  "osen", "останавливать":  "ostanavlivat", "остров":  "ostrov", "отец":  "otec", "открытка":  "otkrytka", "открывать":  "otkryvat", "озеро":  "ozero",
"падать":  "padat", "пахнуть":  "paxnut", "пальто":  "palto", "палец":  "palec", "палец руки":  "palecruki", "парк":  "park", "пациент":  "pacient", "перед":  "pered", "первый":  "pervyj", "песня":  "pesnja", "песок":  "pesok", "петь":  "pet", "письмо":  "pismo", "писать":  "pisat", "пистолет":  "pistolet", "питьпрохлaдный":  "pit", "пиво":  "pivo", "плакать":  "plakat", "пластик":  "plastik", "платье":  "pjatyj", "платить":  "platit", "плавать":  "plavat", "плечо":  "pleco", "плохой":  "ploxoj", "пляж":  "pljaz", "почва": "pocva",
"есть":  "est", "еда":  "eda", "ездить":  "ezdit",
"подарок":  "podarok", "поднимать":  "podnimat", "подписывать":  "podpisyvat", "покупать":  "pokupat", "пол":  "pol", "полиция":  "policija", "половина":  "polovina", "понедельник":  "ponedelnik", "после полудня":  "poslepoludnja", "пот":  "pot", "потолок":  "potolok", "поворачивать":  "povoracivat", "поезд":  "poezd", "пожилой":  "poziloj", "правый":  "pravyj", "президент":  "prezident", "природа":  "priroda", "пробовать": "probovat", "продавать":  "prodavat", "программа":  "programma", "проигрывать":  "proigryvat", "просыпаться":  "prosypatsja", "пройти (мимо)":  "projti", "прямой":  "prjamoj", "прыгать":  "propuskat", "птица":  "ptica", "пять":  "pjat", "пятьдесят":  "pjatdesjat", "пятьдесят два":  "pjatdesjatdva", "пятьдесят один":  "pjatdesjatodin", "пятнадцать":  "pjatnadcat", "пятница":  "pjatnica", "пятно":  "pjatno", "пятый":  "pjatyj", "пыль":  "pyl",
"работа":  "rabota", "работать":  "rabotat", "радио":  "radio", "ранениe":  "ranenye", "раса":  "rasa", "растение":  "rastenie", "рaсти":  "rasti", "ребенок":  "rebenok", "река":  "reka", "религия":  "religija", "репортер":  "reporter", "ресторан":  "restoran", "резать":  "rezat", "рис":  "ris", "рисовать":  "risovat", "родитель":  "roditel", "рот":  "rot", "ровный":  "rovnyj", "розовый":  "rozovyj", "рубашка":  "rubaska", "ручка":  "rucka", "рука":  "ruka", "рыба":  "ryba", "рынок":  "rynok",
"сад":  "sad", "сахар":  "saxar", "самолет":  "samolet", "сантиметр":  "santimetr", "счастливый":  "scastlivyj", "счет":  "scet", "считать":  "scitat", "секретарь":  "sekretar", "секс":  "seks", "секундa":  "sekunda", "семь":  "sem", "семьдесят":  "semdesjat", "семьдесят два":  "semdesjatdva", "семьдесят один":  "semdesjatodin", "семья":  "semja", "семнадцать":  "semnadcat", "семя":  "semja", "сентябрь":  "sentjabr", "сердце":  "serdce", "серебро":  "serebro", "серый":  "seryj", "сестра":  "sestra", "сеть":  "set", "север": "sever", "сезон":  "sezon", "сидеть":  "sidet", "сильный":  "silnyj", "слабый":  "slabyj", "следовать":  "sledovat", "слeпой":  "slepoj", "слеза":  "sleza", "слушать":  "slusat", "слышать":  "slysat", "смерть":  "smert", "смеяться":  "smejatsja", "смотреть":  "smotret", "снаружи":  "snaruzi", "снег":  "sneg", "собака":  "sobaka", "согласный звук":  "soglasnyjzvuk", "сок":  "sok", "соль":  "sol", "солдат":  "soldat", "солнце":  "solnce", "сон":  "son", "сорок":  "sorok", "сорок два":  "sorokdva", "сорок один":  "sorokodin", "сосед":  "sosed", "спальная комната":  "spalnajakomnata", "спать":  "spat", "спина":  "spina", "спорт":  "sport", "среда":  "sreda", "старый":  "staryj", "стекло":  "steklo", "стена":  "stena", "сто":  "sto", "сто десять":  "stodesjat", "сто два":  "stodva", "сто один":  "stoodin", "сто одиннадцать":  "stoodinnadcat", "сто тысяч":  "stotysjac", "стол":  "stol", "сторона":  "storona", "стоять":  "stojat", "страна":  "strana", "стрелять":  "streljat", "строить":  "stroit", "студент":  "student", "стул":  "stul", "суббота":  "subbota", "суд":  "sud", "сухой":  "suxoj", "суп":  "sup", "свадьба":  "svadba", "свет":  "svet", "светлый":  "svetlyj", "свинья":  "svinina", "свинина":  "svinja", "свободный":  "svobodnyj", "священник":  "svjascennik", "сын":  "syn", "сыр":  "syr", "страница":  "stranica",
"шесть":  "sest", "шестьдесят":  "sestdesjat", "шестьдесят два":  "sestdesjatdva", "шестьдесят один":  "sestdesjatodin", "шестнадцать":  "sestnadcat", "шея":  "seja", "шина":  "sina", "широкий":  "sirokij", "школа":  "skola", "шляпа":  "sljapa",
"танцевать":  "tancevat", "тарелка":  "tarelka", "таять":  "tajat", "театр":  "teatr", "технология":  "texnologija", "телефон":  "telefon", "телевизор":  "televizor", "тело":  "telo", "темный":  "temnyj", "температура":  "temperatura", "теплый":  "teplyj", "тесный":  "tesnyj", "тихий":  "tixij", "точка":  "tocka", "толкать":  "tolkat", "толпа":  "tolpa", "толстый":  "tolstyj", "тонкий":  "tonkij", "торт":  "tort", "транспортировка":  "transportirovka", "трава":  "trava", "тренировка":  "trenirovka", "третий":  "tretij", "три":  "tri", "тридцать":  "tridcat", "тридцать два":  "tridcatdva", "тридцать один":  "tridcatodin", "тринадцать":  "trinadcat", "трогать":  "trogat", "трясти":  "trjasti", "туфли":  "tufli", "твeрдый":  "tverdyj", "камень":  "kamen", "ты":  "ty", "тянуть":  "tjanut", "тяжелый":  "tjazelyj", "тысяча":  "tysjaca", "тысяча":  "tysjaca", "тюрьма":  "tjurma",
"целовать":  "celovat", "цена":  "cena", "церковь":  "cerkov", "цвет":  "cvet", "цветок":  "cvetok",
"убивать":  "ubivat", "убийство":  "ubijstvo", "учить":  "ucit", "учиться":  "ucitsja", "учитель":  "ucitel", "угoл":  "ugol", "ухо":  "uxo", "улыбаться":  "ulybatsja", "умереть":  "umeret", "университет":  "universitet", "уродливый":  "urodlivyj", "утро":  "utro", "ужин":  "uzin", "узкий":  "uzkij", "узор":  "obrazec",
"ванная комната":  "vannajakomnata", "вечер":  "vecer", "велосипед":  "velosiped", "вентилятор":  "ventiljator", "верх":  "verx", "вес":  "ves", "весна":  "vesna", "ветер":  "veter", "видеть":  "videt", "вилка":  "vilka", "вино":  "vino", "висеть":  "viset", "вниз":  "vniz", "внутри":  "vnutri", "вода":  "voda", "вокзал":  "vokzal", "волна":  "volna", "волосы":  "volosy", "восемь":  "vosem", "восемьдесят":  "vosemdesjat", "восемьдесят два":  "vosemdesjatdva", "восемьдесят один":  "vosemdesjatodin", "восемнадцать":  "vosemnadcat", "воскресенье":  "voskresene", "восток":  "vostok", "война":  "vojna", "воздух":  "vozdux", "врач":  "vrac", "время":  "vremja", "вступать в брак":  "vstupatvbrak", "вторник":  "vtornik", "второй":  "vtoroj", "вверх":  "vverx", "вы":  "vy", "выборы":  "vybory", "выигрывать":  "vyigryvat", "высокий":  "vysokij", "взрослый":  "vzroslyjcelovek", "взрывать":  "vzryvat",
"я":  "ja", "яблоко":  "jabloko", "яд":  "jad", "ядерный":  "jadernyj", "январь":  "janvar", "яйцо":  "jajco", "язык":  "jazyk",
"юбка":  "jubka", "юг":  "jug", "юрист":  "jurist",
"жара":  "zara", "жаркий":  "zarkij", "желтый":  "zeltyj", "жена":  "zena", "женщина":  "zenscina", "женский":  "zenskij", "жертва":  "zertva", "жить":  "zit", "животное":  "zivotnoe", "живой":  "zivoj", "журнал":  "zurnal",
"зима":  "zima", "злонамеренный":  "nedobrozelatelnyj", "знак":  "znak", "золото":  "zoloto", "зуб":  "zub", "звезда":  "zvezda", "звонить":  "zvonit", "звук":  "zvuk"
  }
}

module.exports = {
  get_lang_map: get_lang_map,
  get_ru_map: get_ru_map
}



